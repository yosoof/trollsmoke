{
  description = "trollsmoke | utilities for everyone";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    kittypkgs = {
      url = "git+https://codeberg.org/caffeinatedcharlie/kittypkgs";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = { self, nixpkgs, flake-utils, kittypkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShells = {
          default = pkgs.mkShell {
            buildInputs = [
              kittypkgs.packages.${pkgs.system}.bun-baseline
              pkgs.nushell
            ];

            shellHook = ''
              echo "welcome to trollsmoke"
            '';
          };
        };
      }
    );
}

